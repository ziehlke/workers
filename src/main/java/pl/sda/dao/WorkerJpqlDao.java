package pl.sda.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import pl.sda.dto.Tasks;
import pl.sda.dto.Worker;

import java.util.List;

public class WorkerJpqlDao implements WorkerDao {


    private SessionFactory sessionFactory;

    public WorkerJpqlDao() {
        sessionFactory = new Configuration().configure().buildSessionFactory();
    }


    @Override
    public List<Worker> getAllWorkes() {
        Session session = sessionFactory.openSession();
        List<Worker> workers = session.createQuery("from Worker").getResultList();
//        List<Worker> workers2 = session.createNamedQuery("getAll", Worker.class).getResultList(); // to samo co wyzej druga opcja
        session.close();
        return workers;
    }


    @Override
    public List<Worker> getByLastName(String lastName) {
        Session session = sessionFactory.openSession();
        List<Worker> workers = session.createNamedQuery("getByLastName", Worker.class)
                .setParameter("lastName", lastName)
                .getResultList();
//
//        List<Worker> workers = session.createQuery("from Worker where lastName = :lastName") // to samo co wyzej tylko bez named query
//                .setParameter("lastName", lastName)
//                .getResultList();
        session.close();
        return workers;
    }

    @Override
    public List<Worker> getFilteredWorkers(String filter) {
        Session session = sessionFactory.openSession();
        int filterInt = Integer.MIN_VALUE;
        try{
            filterInt = Integer.parseInt(filter);
        } catch (NumberFormatException e){
            ;
        }
        List<Worker> workers = session
                .createQuery("from Worker where lastName LIKE concat('%', :filter, '%' ) OR firstName LIKE concat('%', :filter, '%' ) OR position LIKE concat('%', :filter, '%' ) OR salary = :filterInt OR birthYear = :filterInt")
                .setParameter("filter", filter)
                .setParameter("filterInt", filterInt)
                .getResultList();
//        List<Worker> workers2 = session.createNamedQuery("getAll", Worker.class).getResultList(); // to samo co wyzej druga opcja
        session.close();
        return workers;
    }

    @Override
    public Worker getWorker(long idWorker) {
        Session session = sessionFactory.openSession();
        Worker worker = session.get(Worker.class, idWorker);
        session.close();
        return worker;
    }

    @Override
    public void deleteWorker(long workerId) {
        Session session = sessionFactory.openSession();
        Transaction tr = session.beginTransaction();

//        session.delete(getWorker(workerId));
//        session.delete(session.get(Worker.class, workerId)); // to troche bez sensu, bo pobieramy workera, zeby go usunac...
        Worker worker = new Worker();
        worker.setId(workerId);
        session.delete(worker);

        tr.commit();
        session.close();
    }

    @Override
    public void deleteTask(long taskId) {
        // TODO:
    }

    @Override
    public void saveWorker(Worker worker) {
/*        Session session = sessionFactory.openSession();
        session.save(worker);
        session.close();*/

        Session session = sessionFactory.openSession();
        Transaction tr = session.beginTransaction();

        session.save(worker);

        tr.commit();
        session.close();
    }

    @Override
    public void saveTask(Tasks task) {
        // TODO:
    }

    @Override
    public void updateWorker(Worker worker) {
        Session session = sessionFactory.openSession();
        session.update(worker);
        session.close();
    }


}
