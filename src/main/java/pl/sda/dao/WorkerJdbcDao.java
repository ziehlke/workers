package pl.sda.dao;

import pl.sda.db.DBUtil;
import pl.sda.dto.Tasks;
import pl.sda.dto.Worker;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class WorkerJdbcDao implements WorkerDao {

    Connection conn = DBUtil.getConnection();

    @Override
    public List<Worker> getAllWorkes() {
        Statement statement = null;
        List<Worker> workersList = new ArrayList<>();
        try {
            statement = conn.createStatement();

            String query = "select * from worker";
            ResultSet result = statement.executeQuery(query);

            while (result.next()) {
                Worker worker = new Worker(
                        result.getLong("id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("position"),
                        result.getInt("salary"),
                        result.getInt("birthYear")
                );
                workersList.add(worker);
            }
            result.close();
            statement.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return workersList;
    }

    @Override
    public List<Worker> getByLastName(String lastName) {
        // TODO: implement
        return null;
    }

    @Override
    public List<Worker> getFilteredWorkers(String lastName) {
        return null;
    }

    @Override
    public Worker getWorker(long idWorker) {
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Worker worker = new Worker();
        try {
            statement = conn.prepareStatement("SELECT * FROM worker WHERE id=?");
            statement.setLong(1, idWorker);
            resultSet = statement.executeQuery();

            if (resultSet.next()) {
                worker.setId(idWorker);
                worker.setFirstName(resultSet.getString("first_name"));
                worker.setLastName(resultSet.getString("last_name"));
                worker.setPosition(resultSet.getString("position"));
                worker.setSalary(resultSet.getInt("salary"));
                worker.setBirthYear(resultSet.getInt("birthYear"));
            } else {
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return worker;
    }

    @Override
    public void deleteWorker(long workerId) {
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = conn.prepareStatement("DELETE FROM worker WHERE id=?");
            statement.setLong(1, workerId);
            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    @Override
    public void deleteTask(long taskId) {
        // TODO:
    }


    @Override
    public void saveWorker(Worker worker) {
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = conn.prepareStatement("INSERT INTO worker (first_name,last_name,position,salary,birthYear) VALUES (?,?,?,?,?)");
            statement.setString(1, worker.getFirstName());
            statement.setString(2, worker.getLastName());
            statement.setString(3, worker.getPosition());
            statement.setInt(4, worker.getSalary());
            statement.setInt(5, worker.getBirthYear());
            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    @Override
    public void saveTask(Tasks task) {
        // TODO:
    }

    @Override
    public void updateWorker(Worker worker) {
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = conn.prepareStatement(
                    "UPDATE worker " +
                            "SET " +
                            "first_name = ?" +
                            ",last_name = ?, " +
                            "position = ?," +
                            "salary = ?," +
                            "birthYear = ? " +
                            "WHERE id = ?");

            statement.setString(1, worker.getFirstName());
            statement.setString(2, worker.getLastName());
            statement.setString(3, worker.getPosition());
            statement.setInt(4, worker.getSalary());
            statement.setInt(5, worker.getBirthYear());
            statement.setLong(6, worker.getId());
            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
