package pl.sda.dao;

import pl.sda.dto.Tasks;
import pl.sda.dto.Worker;

import java.util.List;

public interface WorkerDao {

    List<Worker> getAllWorkes();

    List<Worker> getByLastName(String lastName);

    List<Worker> getFilteredWorkers(String lastName);

    Worker getWorker(long idWorker);

    void deleteWorker(long workerId);

    void deleteTask(long taskId);

    void saveWorker(Worker worker);

    void saveTask(Tasks task);

    void updateWorker(Worker worker);




}