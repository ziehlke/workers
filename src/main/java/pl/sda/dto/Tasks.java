package pl.sda.dto;

import javax.persistence.*;

@Entity
@Table(name = "tasks")
public class Tasks {

    @Id
    @Column(name = "id")
    private long id;
    @Column(name = "desctiption")
    private String desctiption;
    @Column(name = "estimated_time")
    private String estimatedTime;

    @ManyToOne
    @JoinColumn(name = "worker_id")
    private Worker worker;

    public Tasks() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDesctiption() {
        return desctiption;
    }

    public void setDesctiption(String desctiption) {
        this.desctiption = desctiption;
    }

    public String getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(String estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    public Worker getWorker() {
        return worker;
    }

    public void setWorker(Worker worker) {
        this.worker = worker;
    }
}
