package pl.sda.web;


import pl.sda.dao.WorkerCriteriaDao;
import pl.sda.dao.WorkerDao;
import pl.sda.dto.Tasks;
import pl.sda.dto.Worker;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import java.util.List;
import java.util.Random;

@SessionScoped
@ManagedBean(name = "workerList")
public class WorkersListManagedBean {

    //    private List<Worker> workers = new ArrayList<>();
    private Worker newWorker = new Worker();
    private Worker editWorker = new Worker();
    //    private WorkerDao workerDao = new WorkerJdbcDao();
    //    private WorkerDao workerDao = new WorkerJpqlDao();
    private WorkerDao workerDao = new WorkerCriteriaDao();

    private String filterValue = "";

    private Tasks newTask = new Tasks();

    /*@PostConstruct
    public void init() {
        workers.add(new Worker(1l,"Jan", "Kowalski", "Developer", 5000, 1985));
    }*/


    public List<Worker> getList() {
        return filterValue.isEmpty() ? workerDao.getAllWorkes() : workerDao.getFilteredWorkers(filterValue);
    }

    public void addNewWorker() {
        // dodać pracownika do bazy
        // newWorker.setId((new Random()).nextLong());
        workerDao.saveWorker(newWorker);
        newWorker = new Worker(); // to robimy, aby wyczyscic formularz

        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Nowy pracownik został dodany!"));
    }

    public void addNewTask() {
        newTask.setWorker(editWorker);
        if (editWorker.getTasks().size() > 4) {
            editWorker.getTasks().remove(new Random().nextInt(5));
            // workerDao.deleteTask( editWorker.getTasks().get(0).getId()); // nie trzeba tak, bo mamy cascade
        }
        editWorker.getTasks().add(newTask);

        workerDao.updateWorker(editWorker);
        newTask = new Tasks(); // to robimy, aby wyczyscic formularz

        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Nowe zadanie zostało dodane!"));
    }


    public void deleteWorker(long id) {
        // dodać usuwanie do bazy
        // workers.removeIf(x -> x.getId() == id);
        workerDao.deleteWorker(id);

        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Pracownik został usunięty!"));
    }


    public Worker getNewWorker() {
        return newWorker;
    }


    public String getFilterValue() {
        return filterValue;
    }

    public void setFilterValue(String filterValue) {
        this.filterValue = filterValue;
    }

    public Tasks getNewTask() {
        return newTask;
    }

    public void setNewTask(Tasks newTask) {
        this.newTask = newTask;
    }

    public void editWorker(Long id) {
        editWorker = workerDao.getWorker(id);
    }

    public void getEditWorker(Long id) {
        editWorker = workerDao.getWorker(id);
    }

    public void setEditWorker(Worker editWorker) {
        this.editWorker = editWorker;
    }
}
