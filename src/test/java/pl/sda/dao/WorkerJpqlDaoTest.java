package pl.sda.dao;

import org.junit.Assert;
import org.junit.Test;
import pl.sda.dto.Worker;

import java.util.List;

import static org.junit.Assert.*;

public class WorkerJpqlDaoTest {

    @Test
    public void getAllWorkes() {
        WorkerDao workerDao = new WorkerJpqlDao();
        List<Worker> workers = workerDao.getAllWorkes();
        System.out.println(workers);
        Assert.assertTrue(workers.size() > 0);
    }


    @Test
    public void getWorker() {
        WorkerDao workerDao = new WorkerJpqlDao();
        Worker worker = workerDao.getWorker(1);
        Assert.assertTrue(worker.getPosition().equals("Dyrektor"));
    }


    @Test
    public void getByLastName() {
        WorkerDao workerDao = new WorkerJpqlDao();
        List<Worker> workers = workerDao.getByLastName("Adamczyk");
        System.out.println(workers);
        Assert.assertTrue(workers.size() > 0);
    }
}
