package pl.sda.dao;

import org.junit.Assert;
import org.junit.Test;
import pl.sda.dto.Worker;

import java.util.List;

import static org.junit.Assert.*;

public class WorkerJdbcDaoTest {

    @org.junit.Test
    public void getAllWorkes() {
        WorkerDao workerDao = new WorkerJdbcDao();
        List<Worker> workers = workerDao.getAllWorkes();
        Assert.assertTrue(workers.size() > 0);
    }


    @Test
    public void getWorker() {
        WorkerDao workerDao = new WorkerJdbcDao();
        Worker worker = workerDao.getWorker(1);

        Assert.assertTrue(worker.getPosition().equals("Dyrektor"));
    }
}